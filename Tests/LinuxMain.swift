import XCTest
@testable import gaming_cloud_servicesTests

XCTMain([
     testCase(gaming_cloud_servicesTests.allTests),
])
