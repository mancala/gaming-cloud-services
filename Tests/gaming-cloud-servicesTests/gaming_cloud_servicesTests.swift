import XCTest
@testable import gaming_cloud_services

class gaming_cloud_servicesTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(gaming_cloud_services().text, "Hello, World!")
    }


    static var allTests : [(String, (gaming_cloud_servicesTests) -> () throws -> Void)] {
        return [
            ("testExample", testExample),
        ]
    }
}
