import PackageDescription

let package = Package(
    name: "GamingCloudServices",
    dependencies: [
        .Package(url: "git@gitlab.com:mancala/gaming-core.git", majorVersion: 0, minor: 1),
        .Package(url: "git@gitlab.com:msanford1030/KQueue.git", majorVersion: 1, minor: 1)
    ])
