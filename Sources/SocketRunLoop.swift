//
//  SocketRunLoop.swift
//
//  Created by Michael Sanford on 11/6/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import Darwin
import Dispatch
import GamingCore
import SwiftOnSockets

protocol SocketRunLoopDelegate: AnyObject {
    func runLoop(_ runLoop: SocketRunLoop, socketDidReceiveData: UDPServerSocket)

    func runLoop(_ runLoop: SocketRunLoop, socket: TCPServerSocket, didReceiveConnectionRequestFrom clientSocket: TCPClientSocket)
    func runLoop(_ runLoop: SocketRunLoop, socket: TCPServerSocket, didCloseWithError: SocketErrorCode?)
    
    func runLoop(_ runLoop: SocketRunLoop, socket: TCPClientSocket, didReceiveDataWithContext context: Any?)
    func runLoop(_ runLoop: SocketRunLoop, socket: TCPClientSocket, didCloseWithError: SocketErrorCode?, context: Any?)
}

fileprivate enum CallbackInfo {
    case tcpClient(socket: TCPClientSocket, context: Any?)
    case tcpServer(socket: TCPServerSocket)
    case udpServer(socket: UDPServerSocket)
    
    var socket: Socket {
        switch self {
        case .tcpClient(let socket, _):
            return socket
        case .tcpServer(let socket):
            return socket
        case .udpServer(let socket):
            return socket
        }
    }
}


class SocketRunLoop {
    private enum State {
        case running
        case stopping
        case stopped
    }
    
    private var emptyByte = UInt8(0)
    private weak var delegate: SocketRunLoopDelegate?
    private var kqueueId: Int32
    private var socketList: Set<kevent>
    private let queue: DispatchQueue
    private var isDirty: Bool = false
    private var state: State
    private var callbackInfos: [SocketRawHandle: CallbackInfo]
    private var eventHandle: SocketRawHandle
    private var triggerHandle: SocketRawHandle
    
    init?() {
        let kqueueId = kqueue()
        guard kqueueId != -1 else { return nil }
        self.kqueueId = kqueueId
        state = .stopped
        queue = DispatchQueue.init(label: "SocketRunLoop")
        callbackInfos = [:]
//        self.delegate = delegate
        
        var socketPair: [SocketRawHandle]
        socketPair = Array(repeating: SocketRawHandle(0), count: 2)
        socketpair(PF_LOCAL, SOCK_STREAM, 0, &socketPair)
        eventHandle = socketPair[0]
        triggerHandle = socketPair[1]
        
        socketList = Set<kevent>()
        var ev = kevent()
        ev.set(ident: UInt(eventHandle), filter: Int16(EVFILT_READ), flags: UInt16(EV_ADD | EV_CLEAR), fflags: 0, data: 0)
        socketList.insert(ev)
    }
    
    private func add(socketWithCallbackInfo callbackInfo: CallbackInfo) throws {
        guard let handle = callbackInfo.socket.handle else { throw SocketError.socketClosed }

        let ptr = UnsafeMutablePointer<CallbackInfo>.allocate(capacity: 1)
        ptr.initialize(to: callbackInfo)
        
        var ev = kevent()
        ev.set(ident: UInt(handle), filter: Int16(EVFILT_READ), flags: UInt16(EV_ADD | EV_CLEAR), fflags: 0, data: 0, udata: UnsafeMutableRawPointer(ptr))
        queue.sync {
            callbackInfos[handle] = callbackInfo
            socketList.insert(ev)
            isDirty = true
        }
        
        // trigger event in run loop
        guard case .running = state else { return }
        _ = write(triggerHandle, &emptyByte, 1)
    }
    
    func add(socket: UDPServerSocket) throws {
        let callbackInfo = CallbackInfo.udpServer(socket: socket)
        try add(socketWithCallbackInfo: callbackInfo)
    }
    
    func add(socket: TCPServerSocket) throws {
        let callbackInfo = CallbackInfo.tcpServer(socket: socket)
        try add(socketWithCallbackInfo: callbackInfo)
    }
    
    func add(socket: TCPClientSocket, context: Any?) throws {
        let callbackInfo = CallbackInfo.tcpClient(socket: socket, context: context)
        try add(socketWithCallbackInfo: callbackInfo)
    }
    
    func remove(socket: TCPClientSocket) throws {
        guard let handle = socket.handle else { throw SocketError.socketClosed }
        if case .stopping = state { return }
        queue.sync {
            callbackInfos[handle] = nil
            var ev = kevent()
            ev.set(ident: UInt(handle), filter: 0, flags: 0, fflags: 0, data: 0)
            socketList.remove(ev)
            isDirty = true
        }
        
        // trigger event in run loop
        if case .running = state {
            _ = write(triggerHandle, &emptyByte, 1)
        }
    }
    
    func run(withDelegate delegate: SocketRunLoopDelegate) {
        self.delegate = delegate
        
        queue.sync {
            state = .running
        }
        
        var socketListCopy: [kevent] = []
        var socketCount: Int32 = 0
        var isRunning = true
        while isRunning {
            queue.sync {
                guard case .running = state else {
                    isRunning = false
                    return
                }
                guard isDirty else { return }
                
                socketCount = Int32(socketList.count)
                socketListCopy = Array<kevent>(socketList)
                isDirty = false
            }
            guard isRunning else { continue }
            
            var eventList: [kevent] = Array(repeating: kevent(), count: Int(socketCount))
            let result = kevent(kqueueId, &socketListCopy, socketCount, &eventList, socketCount, nil)
            
            guard result != -1 else {
                log(.warning, domain: "SocketRunLoop", log: "kevent ran into error")
                return
            }
            
            guard result != 0 else { continue }
            
            let subEventList = eventList[0 ..< Int(result)]
            subEventList.forEach { event in
                guard event.ident != 0 else { return }
                guard event.ident != UInt(eventHandle) else {
                    var buffer: UInt8 = 0
                    _ = read(eventHandle, &buffer, 1)
                    return
                }
                
                let eventFlags = Int32(event.flags)
                let socketHandle = SocketRawHandle(event.ident)
                let callbackInfo = event.udata.bindMemory(to: CallbackInfo.self, capacity: 1).pointee
                print("result=\(result), eventFlags=\(eventFlags)")
                if (eventFlags & EV_EOF) != 0 {
                    // close socket do to disconnect
                    queue.sync {
                        callbackInfos[socketHandle] = nil
                        socketList.remove(event)
                        isDirty = true
                    }
                    switch callbackInfo {
                    case .tcpClient(let socket, let context):
                        self.delegate?.runLoop(self, socket: socket, didCloseWithError: nil, context: context)
                    case .tcpServer(let socket):
                        self.delegate?.runLoop(self, socket: socket, didCloseWithError: nil)
                    case .udpServer: ()
                    }
                } else if (eventFlags & EVFILT_READ) != 0 {
                    // read
                    switch callbackInfo {
                    case .tcpClient(let socket, let context):
                        self.delegate?.runLoop(self, socket: socket, didReceiveDataWithContext: context)
                    case .tcpServer(let socket):
                        do {
                            guard let clientSocket = try socket.accept() else {
                                self.delegate?.runLoop(self, socket: socket, didCloseWithError: SocketErrorCode.current)
                                isRunning = false
                                return
                            }
                            
                            self.delegate?.runLoop(self, socket: socket, didReceiveConnectionRequestFrom: clientSocket)
                            
                        } catch {
                            self.delegate?.runLoop(self, socket: socket, didCloseWithError: SocketErrorCode.current)
                            isRunning = false
                        }
                    case .udpServer(let socket):
                        self.delegate?.runLoop(self, socketDidReceiveData: socket)
                    }
                    
                } else {
                    // error
                    log(.warning, domain: "SocketRunLoop", log: "shouldn't get here")
                }
            }
            
        }
        
        queue.sync {
            state = .stopped
        }
        
    }

    func stop(withCloseAllEnabled closeAllEnabled: Bool = true) {
        queue.sync {
            state = .stopping
            if closeAllEnabled {
                do {
                    try callbackInfos.values.forEach { try $0.socket.close() }
                } catch (let error) {
                    log(.warning, domain: "SocketRunLoop", log: "error on close = \(error)")
                }
            }
        }
        
        let waitQueue = DispatchQueue(label: "waitQueue")
        waitQueue.sync {
            var isStopped = false
            while !isStopped {
                Thread.sleep(forTimeInterval: 0.005)
                self.queue.sync {
                    if case .stopped = self.state {
                        isStopped = true
                    }
                }
            }
        }
    }
}

/*---------------------------------------------------------------*/

extension kevent: Hashable {
    mutating func set(ident: UInt, filter: Int16, flags: UInt16, fflags: UInt32, data: Int, udata: UnsafeMutableRawPointer? = nil) {
        self.ident = ident
        self.filter = filter
        self.flags = flags
        self.fflags = fflags
        self.data = data
        self.udata = udata
    }
    
    public var hashValue: Int {
        return Int(ident)
    }
}

public func ==(lhs: kevent, rhs: kevent) -> Bool {
    return lhs.ident == rhs.ident
}
