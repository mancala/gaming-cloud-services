//
//  EchoService.swift
//
//  Created by Michael Sanford on 9/28/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import GamingCore
import SwiftOnSockets

public class EchoService {
    
    public func startUsingBlockingTCP() throws {
        do {
            guard let serverSocket = TCPServerSocket(port: 8081) else { throw SocketError.internalError }
            print("TCP server socket: created")
            
            try serverSocket.bind()
            print("TCP server socket: bound")
            
            try serverSocket.listen()
            print("TCP server socket: started to listen")
            
            guard let clientSocket = try serverSocket.accept() else {
                print("server socket closed. that's unexpected!!")
                return
            }
            print("TCP server socket: accepted")
            
            guard let receivedData = try clientSocket.read(numberOfBytes: 255) else {
                print("lost client conenction")
                return
            }
            
            print("TCP server socket: read data")
            try clientSocket.write(receivedData)
            
            print("TCP client socket: write data")
            try clientSocket.close()
            print("TCP client socket: closed")
            
            try serverSocket.close()
            print("TCP server socket: closed")
            
        } catch (let error) {
            print("error=\(error)")
            throw error
        }
    }
    
    
    private var service: NetworkService?
    private var serviceDelegate: EchoServiceDelegate?
    
    private class EchoServiceDelegate: RealTimeServiceDelegate {
        func service(_ service: NetworkService, didConnectToClient token: ClientToken) {
            print("\(service.connectionType) server socket: accepted")
        }
        func service(_ service: NetworkService, didDisconnectFromClient token: ClientToken) {
            print("\(service.connectionType) client socket: closed")
        }
        func service(_ service: NetworkService, didReceiveMessage message: Message, fromClient token: ClientToken) {
            do {
                try service.send(message: message, toClient: token)
                print("\(service.connectionType) client socket: write data (isMain=\(OperationQueue.current == OperationQueue.main))")
                
                try service.disconnect(fromClient: token)
                print("\(service.connectionType) client socket: disconnect (isMain=\(OperationQueue.current == OperationQueue.main))")
                
//                    try service.stop()
//                    print("TCP server socket: closed (isMain=\(OperationQueue.current == OperationQueue.main))")
                
            } catch (let error) {
                print("service send error: \(error)")
            }
        }
        func service(_ service: RealTimeService, didReceiveStream data: StreamData, fromClient token: ClientToken) {
            print("real time client socket received stream")
        }
        
        func serviceBroadcastStreamInfo(_ service: RealTimeService) -> [BroadcastStreamInfo] {
            return []
        }
    }
    
    private func start(type: ConnectionType, port: PortID) throws {
        serviceDelegate = EchoServiceDelegate()
        
        let service: NetworkService
        switch type {
        case .tcp:
            let tcpService = try EventBasedService(tcpPort: port)
            tcpService.delegate = serviceDelegate
            service = tcpService
        case .udp:
            let udpService = try RealTimeService(udpPort: port, ssrc: 123)
            udpService.delegate = serviceDelegate
            service = udpService
        }
        
        try service.start()
        self.service = service
    }

    public func startUsingEventBasedService() throws {
        let eventBasedService = try EventBasedService(tcpPort: 8083)
        service = eventBasedService
        serviceDelegate = EchoServiceDelegate()
        eventBasedService.delegate = serviceDelegate
        try eventBasedService.start()
    }
    
    public func startUsingRealTimeService() throws {
        let realTimeService = try RealTimeService(udpPort: 8083, ssrc: 123)
        service = realTimeService
        serviceDelegate = EchoServiceDelegate()
        realTimeService.delegate = serviceDelegate
        try realTimeService.start()
    }
    
    public func startUsingUDP() throws {
        do {
            guard let serverSocket = UDPServerSocket(port: 30716) else { throw SocketError.internalError }
            print("UDP socket: created")
            try serverSocket.bind()
            print("UDP socket: bound")
            guard let (data, sender) = try serverSocket.receive(numberOfBytes: 255) else { return }
            print("UDP socket: received data")
            try serverSocket.send(data, to: sender)
            print("UDP socket: sent data")
            try serverSocket.close()
            print("UDP socket: closed")
        } catch (let error) {
            print("error=\(error)")
            throw error
        }
    }
    
}



/*
 int main(int argc, char *argv[])
 {
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0)
         error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
         error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
     if (newsockfd < 0)
        error("ERROR on accept");
     bzero(buffer,256);
     n = read(newsockfd,buffer,255);
     if (n < 0) error("ERROR reading from socket");
     printf("Here is the message: %s\n",buffer);
     n = write(newsockfd,"I got your message",18);
     if (n < 0) error("ERROR writing to socket");
     close(newsockfd);
     close(sockfd);
     return 0;
 }
 */
