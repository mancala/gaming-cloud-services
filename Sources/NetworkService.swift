//
//  Service.swift
//
//  Created by Michael Sanford on 5/21/16.
//  Copyright © 2016 infinity point. All rights reserved.
//

/****
 * TODO's
 * - Provide a "RealTimeSession and EventBasedSession" instead of ServiceConnection w a flag
 * - Add a "GameRealTimeSession"
 * - Refactor MancalaGameClient that uses new "sessions" API
 * - Fix "disconnect callback not working" on ServiceConnection
 * - Add stream/game timer to client
 * - Create lobby
 * - Load game engine view bundle/path config
 ****/
 
import Foundation
import Dispatch
import GamingCore
import SwiftOnSockets

public enum ServiceError: Error {
    case invalidClientToken
    case internalError(message: String)
    case broadcastError(errors: [(token: ClientToken, error: ServiceError)])
}

public struct BroadcastStreamInfo {
    public let streamData: StreamData?
    public let clientTokens: [ClientToken]
    
    public init(streamData: StreamData?, clientTokens: [ClientToken]) {
        self.streamData = streamData
        self.clientTokens = clientTokens
    }
}

/*-----------------------------------------------------------------------------------*/

private let ServiceDomain = "Service"

fileprivate protocol ServerDelegate: AnyObject {
    func onDidConnect(withClientToken token: ClientToken)
    func onDidDisconnect(withClientToken token: ClientToken)
    func onDidReceiveKeepAlive(fromClient token: ClientToken)
    func onDidReceivePong(withDuration: Timestamp32, fromClient token: ClientToken)
    func onDidReceive(message: Message, fromClient token: ClientToken)
    func onDidReceive(stream: StreamData, fromClient token: ClientToken)
}

fileprivate protocol Server: SocketRunLoopDelegate {
    var port: PortID { get }
    var connectionType: ConnectionType { get }
    var delegate: ServerDelegate? { get set }
    
    func start(withRunLoop runLoop: SocketRunLoop) throws
    func stop() throws
    
    func send(_ message: Message, toClient token: ClientToken) throws
    func sendPing(toClient token: ClientToken) throws
    func sendKeepAlive(toClient token: ClientToken) throws
    func disconnect(fromClient token: ClientToken) throws
}

/*-------------------*/

public protocol NetworkServiceDelegate: AnyObject {
    func service(_ service: NetworkService, didConnectToClient token: ClientToken)
    func service(_ service: NetworkService, didDisconnectFromClient token: ClientToken)
    func service(_ service: NetworkService, didReceiveMessage: Message, fromClient token: ClientToken)
}

public class NetworkService: ServerDelegate {
    public let name: String
    public let callbackQueue: DispatchQueue
    fileprivate weak var _delegate: NetworkServiceDelegate?
    fileprivate let server: Server
    
    private let incomingQueue: DispatchQueue
    fileprivate var processQueue: DispatchQueue
    fileprivate var runLoop: SocketRunLoop?
    fileprivate var pings: [ClientToken: (Timestamp32) -> Void]
    
    fileprivate init(server: Server, name: String? = nil, callbackQueue: DispatchQueue) {
        self.name = name ?? "Service.\(UInt16(truncatingBitPattern: Date.now32))"
        self.callbackQueue = callbackQueue
        self.server = server
        self.pings = [:]
        
        incomingQueue = DispatchQueue(label: "\(self.name).incomingQueue")
        processQueue = DispatchQueue(label: "\(self.name).processQueue")
        
        server.delegate = self
    }
    
    public var connectionType: ConnectionType {
        return server.connectionType
    }
    
    public func start() throws {
        try processQueue.sync {
            guard let runLoop = SocketRunLoop() else {
                log(.warning, domain: ServiceDomain, log: "failed to create socket run loop")
                return
            }
            self.runLoop = runLoop
            try self.server.start(withRunLoop: runLoop)
            
            self.incomingQueue.async {
                runLoop.run(withDelegate: self.server)
            }
        }
    }
    
    public func stop() throws {
        try processQueue.sync {
            try self.server.stop()
        }
    }
    
    public func disconnect(fromClient token: ClientToken) throws {
        try processQueue.sync {
            try self.server.disconnect(fromClient: token)
        }
    }
    
    func sendKeepAlive(toClient token: ClientToken) {
        processQueue.async {
            do {
                try self.server.sendKeepAlive(toClient: token)
            } catch {
                log(.warning, domain: ServiceDomain, log: "failed to perform sendKeepAlive (\(error))")
            }
        }
    }

    public func sendPing(toClient token: ClientToken, completion: @escaping (Timestamp32) -> Void) throws {
        processQueue.async {
            do {
                guard self.pings[token] == nil else { throw ServiceError.internalError(message: "ping already in progress") }
                self.pings[token] = completion
                try self.server.sendPing(toClient: token)
            } catch {
                log(.warning, domain: ServiceDomain, log: "failed to perform sendPing (\(error))")
            }
        }
    }
    
    public func send(message: Message, toClient token: ClientToken) throws {
        try processQueue.sync {
            try self.server.send(message, toClient: token)
        }
    }
    
    public func broadcast(message: Message, toClients tokens: [ClientToken]) {
        broadcast(messages: [message], toClients: tokens)
    }
    
    public func broadcast(messages: [Message], toClients tokens: [ClientToken]) {
        processQueue.async {
            do {
                try messages.forEach { message in
                    try tokens.forEach { try self.server.send(message, toClient: $0) }
                }
            } catch {
                log(.warning, domain: ServiceDomain, log: "error during attempt to broadcast messages (error=\(error))")
            }
        }
    }
    
    fileprivate func onDidReceivePong(withDuration duration: Timestamp32, fromClient token: ClientToken) {
        processQueue.async {
            guard let completion = self.pings[token] else {
                log(.warning, domain: ServiceDomain, log: "pong but not ping (\(token.remoteAddress))")
                return
            }
            self.pings[token] = nil
            self.callbackQueue.async {
                completion(duration)
            }
        }
    }
    
    fileprivate func onDidConnect(withClientToken token: ClientToken) {
        log(.info, domain: ServiceDomain, log: "Service has new client. (name=\(name), address=\(token.remoteAddress))")
        
        callbackQueue.async {
            self._delegate?.service(self, didConnectToClient: token)
        }
    }
    
    fileprivate func onDidDisconnect(withClientToken token: ClientToken) {
        log(.info, domain: ServiceDomain, log: "Service dropped client. (name=\(name), address=\(token.remoteAddress))")
        
        callbackQueue.async {
            self._delegate?.service(self, didDisconnectFromClient: token)
        }
    }
    
    fileprivate func onDidReceiveKeepAlive(fromClient token: ClientToken) {
        log(.info, domain: ServiceDomain, log: "Service did receive keep alive. (name=\(name), address=\(token.remoteAddress))")
    }
    
    fileprivate func onDidReceive(message: Message, fromClient token: ClientToken) {
        log(.info, domain: ServiceDomain, log: "Service did receive message. (name=\(name), address=\(token.remoteAddress))")
        
        callbackQueue.async {
            self._delegate?.service(self, didReceiveMessage: message, fromClient: token)
        }
    }
    
    fileprivate func onDidReceive(stream: StreamData, fromClient token: ClientToken) {
    }
}

/*-------------------*/

public protocol RealTimeServiceDelegate: NetworkServiceDelegate {
    func service(_ service: RealTimeService, didReceiveStream data: StreamData, fromClient token: ClientToken)
    
    /// May be called on background thread (i.e. not main thread/dispatch queue
    func serviceBroadcastStreamInfo(_ service: RealTimeService) -> [BroadcastStreamInfo]
}

public class RealTimeService: NetworkService {
    
    public enum MessageSendStrategy {
        case interlace
        case immediate
    }

    public enum StreamRate {
        case slowest, slow, medium, fast, fastest
    }
    
    fileprivate struct MessageQueue: SequentialCollection {

        private var highPriorityQueue = Queue<Data>()
        private var mediumPriorityQueue = Queue<Data>()
        private var lowPriorityQueue = Queue<Data>()
        
        var count: Int {
            return highPriorityQueue.count + mediumPriorityQueue.count + lowPriorityQueue.count
        }
        
        var peek: Data? {
            return highPriorityQueue.peek ?? mediumPriorityQueue.peek ?? lowPriorityQueue.peek
        }
        
        var ordered: [Data] {
            return highPriorityQueue.ordered + mediumPriorityQueue.ordered + lowPriorityQueue.ordered
        }
        
        mutating func add(_ packet: Data) {
            highPriorityQueue.add(packet)
            mediumPriorityQueue.add(packet)
            lowPriorityQueue.add(packet)
        }
        
        @discardableResult mutating func remove() -> Data? {
            return highPriorityQueue.remove() ?? mediumPriorityQueue.remove() ?? lowPriorityQueue.remove()
        }
        
        mutating func add(_ packets: [Data]) {
            packets.forEach { add($0) }
        }
    }
    
    fileprivate class UDPServer: Server {
        
        struct ClientContext {
            let token: ClientToken
            let session: RTPSession
            
            init(remoteAddress: IPAddress, ssrc: RTPSourceIdentifier) {
                token = ClientToken(remoteAddress: remoteAddress)
                self.session = RTPSession(ssrc: ssrc)
            }
        }
        
        weak var delegate: ServerDelegate?
        private var runLoop: SocketRunLoop?
        private let socket: UDPServerSocket
        let connectionTimeout: TimeInterval
        private var pingSession: RTPSession?
        private let ssrc: RTPSourceIdentifier
        private var clientContexts: [IPAddress: ClientContext] = [:]
        
        init(port: PortID, connectionTimeout: TimeInterval, session: RTPSession) throws {
            guard connectionTimeout > 1 && connectionTimeout < 30 else {
                throw ServiceError.internalError(message: "invalid connectionTimeout")
            }
            guard let socket = UDPServerSocket(port: port, bufferSize: RTPPacketByteSize) else {
                throw ServiceError.internalError(message: "could not create udp socket")
            }
            socket.isNonBlockingEnabled = true
            self.socket = socket
            self.connectionTimeout = connectionTimeout
            self.ssrc = session.archiver.ssrc
        }
        
        public var port: PortID {
            return socket.port
        }
        
        public var connectionType: ConnectionType {
            return .udp
        }
        
        public func start(withRunLoop runLoop: SocketRunLoop) throws {
            try socket.bind()
            print("\(ConnectionType.udp) server: bound")
            
            try runLoop.add(socket: socket)
            self.runLoop = runLoop
        }
        
        public func stop() throws {
            try socket.close()
            print("UDP server: stopped")
            
            clientContexts = [:]
        }
        
        public func sendKeepAlive(toClient token: ClientToken) throws {
            guard let clientContext = clientContexts[token.remoteAddress] else { throw ServiceError.invalidClientToken }
            let packet = clientContext.session.archiver.keepAlivePacket
            try socket.send(packet, to: token.remoteAddress)
        }
        
        public func sendPing(toClient token: ClientToken) throws {
            guard let clientContext = clientContexts[token.remoteAddress] else { throw ServiceError.invalidClientToken }
            let pingPacket = clientContext.session.archiver.pingPacket
            try socket.send(pingPacket, to: token.remoteAddress)
            print("UDP service: sent ping")
        }
        
        public func send(_ message: Message, toClient token: ClientToken) throws {
            guard let clientContext = clientContexts[token.remoteAddress] else { throw ServiceError.invalidClientToken }
            let packets = clientContext.session.archiver.messagePackets(forMessage: message)
            try packets.forEach { try socket.send($0, to: token.remoteAddress) }
            print("UDP service: sent message packets")
        }
        
        public func send(_ packet: Data, toClient token: ClientToken) throws {
            try socket.send(packet, to: token.remoteAddress)
            print("UDP service: sent message packet")
        }
        
        public func send(_ streamData: StreamData, toClient token: ClientToken) throws {
            guard let clientContext = clientContexts[token.remoteAddress] else { throw ServiceError.invalidClientToken }
            let packet = clientContext.session.archiver.streamPacket(for: streamData)
            try socket.send(packet, to: token.remoteAddress)
            print("UDP service: sent stream packet")
        }
        
        public func broadcastKeepAlive(toClients tokens: [ClientToken]? = nil) throws {
            if let tokens = tokens {
                try tokens.forEach { try self.sendKeepAlive(toClient: $0) }
            } else {
                try clientContexts.forEach { (_, clientContext) in
                    try self.sendKeepAlive(toClient: clientContext.token)
                }
            }
        }
        
        public func broadcastPackets(for message: Message, toClients tokens: [ClientToken]) throws -> [ClientToken: [Data]] {
            return try tokens.reduce([:]) { (result, token) -> [ClientToken: [Data]] in
                guard let clientContext = clientContexts[token.remoteAddress] else { throw ServiceError.invalidClientToken }
                let packets = clientContext.session.archiver.messagePackets(forMessage: message)
                var newResult = result
                newResult[token] = packets
                return newResult
            }
        }
        
        public func broadcast(_ message: Message, toClients tokens: [ClientToken]) throws {
            try tokens.forEach { try send(message, toClient: $0) }
        }
        
        public func broadcast(_ streamData: StreamData, toClients tokens: [ClientToken]) throws {
            guard streamData.payload.count <= RTPPayloadByteSize else { throw ServiceError.internalError(message: "payload too big") }
            let myClientContexts: [ClientContext] = try tokens.flatMap {
                guard let clientContext = self.clientContexts[$0.remoteAddress] else { throw ServiceError.invalidClientToken }
                return clientContext
            }
            try myClientContexts.forEach {
                let streamPacket = $0.session.archiver.streamPacket(for: streamData)
                try socket.send(streamPacket, to: $0.token.remoteAddress)
            }
            print("UDP service: sent stream packets")
        }
        
        public func disconnect(fromClient token: ClientToken) throws {
            guard clientContexts[token.remoteAddress] != nil else { throw ServiceError.invalidClientToken }
            clientContexts[token.remoteAddress] = nil
            print("UDP service: disconnected client")
            // TODO: send 'disconnect' packet to client here
        }
        
        private func onDidUdpClientConnect(_ clientContext: ClientContext) {
            clientContexts[clientContext.token.remoteAddress] = clientContext
            let token = clientContext.token
            delegate?.onDidConnect(withClientToken: token)
        }
        
        private func onDidUdpClientDisconnect(_ token: ClientToken) {
            clientContexts[token.remoteAddress] = nil
            delegate?.onDidDisconnect(withClientToken: token)
        }
        
        private func onDidReceiveUDPPacket(from socket: UDPServerSocket) {
            // monitor for new incoming data
            var readEnabled = true
            while readEnabled {
                do {
                    guard let (data, remoteAddress) = try socket.receive(numberOfBytes: RTPPacketByteSize) else {
                        readEnabled = false
                        continue
                    }
                    
                    let clientContext: ClientContext
                    if let context = clientContexts[remoteAddress] {
                        clientContext = context
                    } else {
                        // new connection
                        clientContext = ClientContext(remoteAddress: remoteAddress, ssrc: ssrc)
                        onDidUdpClientConnect(clientContext)
                    }
                    
                    // incoming data
                    do {
                        let result = try clientContext.session.unarchiver.unarchive(packet: data)
                        switch result {
                        case .keepAlive:
                            delegate?.onDidReceiveKeepAlive(fromClient: clientContext.token)
                        case .message(let message):
                            delegate?.onDidReceive(message: message, fromClient: clientContext.token)
                        case .ping(let timestamp):
                            let pongPacket = clientContext.session.archiver.pongPacket(withTimestamp: timestamp)
                            try socket.send(pongPacket, to: clientContext.token.remoteAddress)
                        case .pong(let duration):
                            delegate?.onDidReceivePong(withDuration: duration, fromClient: clientContext.token)
                        case .stream(let streamData):
                            delegate?.onDidReceive(stream: streamData, fromClient: clientContext.token)
                        case .messageFragment, .messageFragmentDOA, .streamDOA:
                            ()
                        }
//                        try delegate?.onDidReceivePacket(data: data, context: clientContext)
                    } catch {
                        log(.warning, domain: ServiceDomain, log: "udp socket error during packet processing. error=\(error)")
                        
                        // close connection- something went wrong
                        onDidUdpClientDisconnect(clientContext.token)
                        readEnabled = false
                    }
                    
                } catch {
                    log(.warning, domain: ServiceDomain, log: "udp socket error during read. error=\(error)")
                    // close connection- something went wrong
                    readEnabled = false
                }
            }
        }
        
        func runLoop(_ runLoop: SocketRunLoop, socket: TCPServerSocket, didReceiveConnectionRequestFrom clientSocket: TCPClientSocket) {}
        func runLoop(_ runLoop: SocketRunLoop, socket: TCPServerSocket, didCloseWithError errorCode: SocketErrorCode?) {}
        func runLoop(_ runLoop: SocketRunLoop, socket: TCPClientSocket, didReceiveDataWithContext context: Any?) {}
        func runLoop(_ runLoop: SocketRunLoop, socket: TCPClientSocket, didCloseWithError errorCode: SocketErrorCode?, context: Any?) {}
        
        func runLoop(_ runLoop: SocketRunLoop, socketDidReceiveData socket: UDPServerSocket) {
            onDidReceiveUDPPacket(from: socket)
        }
        
        func runLoop(_ runLoop: SocketRunLoop, socket: UDPServerSocket, didCloseWithError errorCode: SocketErrorCode?) {
            if let errorCode = errorCode {
                log(.warning, domain: ServiceDomain, log: "server socket error (errorCode=\(errorCode))")
            }
        }
    }

    private let domain = "RealTimeService"
    private(set) var streamRate: StreamRate = .medium
    private(set) var messageSendStrategy: MessageSendStrategy = .immediate
    private var streamTimer: DispatchSourceTimer
    private var messageTimer: DispatchSourceTimer
    private var isMessageTimerActive = false
    private var isMessageTick = false
    fileprivate var realTimeServer: UDPServer
    private var messagePacketQueue = [ClientToken: MessageQueue]()
    
    public weak var delegate: RealTimeServiceDelegate? {
        didSet {
            _delegate = delegate
        }
    }
    
    public init(udpPort port: PortID,
         connectionTimeout: TimeInterval = 5,
         name: String? = nil,
         ssrc: RTPSourceIdentifier,
         callbackQueue: DispatchQueue = DispatchQueue.main) throws {
        
        realTimeServer = try UDPServer(port: port, connectionTimeout: connectionTimeout, session: RTPSession(ssrc: ssrc))
        
        let queue = DispatchQueue(label: "streamTimerQueue", attributes: [])
        streamTimer = DispatchSource.makeTimerSource(flags: [], queue: queue)
        messageTimer = DispatchSource.makeTimerSource(flags: [], queue: queue)
        super.init(server: realTimeServer, name: name, callbackQueue: callbackQueue)
    }
    
    override fileprivate func onDidReceive(stream: StreamData, fromClient token: ClientToken) {
        log(.info, domain: ServiceDomain, log: "Service did receive stream data. (name=\(name), address=\(token.remoteAddress)")
        
        callbackQueue.async {
            self.delegate?.service(self, didReceiveStream: stream, fromClient: token)
        }
    }
    
    private func onSendMessagePacketTick() {
        guard !messagePacketQueue.isEmpty else { return }
        
        print("*** [\(Date.now32)] message client count = \(self.messagePacketQueue.count)")
        var updatedMessagePacketQueue: [ClientToken: MessageQueue] = [:]
        
        messagePacketQueue.forEach { (token: ClientToken, queue: MessageQueue) in
            print("*** [\(Date.now32)] message queue count = \(queue.count)")
            var updatedQueue = queue
            guard let messagePacket = updatedQueue.remove() else {
                log(.warning, domain: self.domain, log: "expected message data packet but none found")
                return
            }
            
            if !updatedQueue.isEmpty {
                updatedMessagePacketQueue[token] = updatedQueue
            }
            
            do {
                try self.realTimeServer.send(messagePacket, toClient: token)
            } catch {
                log(.warning, domain: self.domain, log: "attempt to broadcast message packets failed (error=\(error))")
            }
        }
        
        messagePacketQueue = updatedMessagePacketQueue
    }
    
    private func onStreamTick() {
        guard let streamInfo = self.delegate?.serviceBroadcastStreamInfo(self) else { return }
        
        do {
            try streamInfo.forEach {
                guard let streamData = $0.streamData else {
                    try $0.clientTokens.forEach { try self.realTimeServer.sendKeepAlive(toClient: $0) }
                    return
                }
                try $0.clientTokens.forEach { try self.realTimeServer.send(streamData, toClient: $0) }
            }
        } catch {
            log(.warning, domain: ServiceDomain, log: "failed to broadcast stream data (error=\(error))")
        }
    }
    
    override public func send(message: Message, toClient token: ClientToken) {
        broadcast(messages: [message], toClients: [token])
    }
    
    override public func broadcast(messages: [Message], toClients tokens: [ClientToken]) {
        guard !tokens.isEmpty else { return }
        
        self.processQueue.async {
            do {
                try messages.forEach { message in
                    let clientPackets = try self.realTimeServer.broadcastPackets(for: message, toClients: tokens)
                    
                    clientPackets.forEach { (token: ClientToken, packets: [Data]) in
                        var clientMessageQueue = self.messagePacketQueue[token] ?? MessageQueue()
                        clientMessageQueue.add(packets)
                        self.messagePacketQueue[token] = clientMessageQueue
                    }
                }
            } catch {
                log(.warning, domain: self.domain, log: "error when attempting to broadcast message (error=\(error))")
            }
        }
    }
    
    public func start(withStreamRate streamRate: StreamRate, messageSendStrategy: MessageSendStrategy) throws {
        try super.start()
        
        self.streamRate = streamRate
        self.messageSendStrategy = messageSendStrategy
        
        // start streaming
        streamTimer.scheduleRepeating(deadline: .now() + .milliseconds(clockRate), interval: .milliseconds(clockRate), leeway: .milliseconds(clockRateLeeway))
        
        switch messageSendStrategy {
        case .immediate:
            streamTimer.setEventHandler {
                self.onStreamTick()
            }
            messageTimer.setEventHandler {
                self.onSendMessagePacketTick()
            }
            messageTimer.scheduleRepeating(deadline: .now(), interval: .milliseconds(100), leeway: .milliseconds(5))
            isMessageTimerActive = true
            
            processQueue.async {
                self.messageTimer.resume()
            }
            
        case .interlace:
            streamTimer.setEventHandler {
                if self.isMessageTick {
                    self.isMessageTick = false
                    self.onSendMessagePacketTick()
                } else {
                    self.isMessageTick = true
                    self.onStreamTick()
                }
            }
        }
        
        processQueue.async {
            self.streamTimer.resume()
        }
    }
    
    override public func start() throws {
        try start(withStreamRate: .medium, messageSendStrategy: .immediate)
    }
    
    override public func stop() throws {
        try super.stop()
        streamTimer.cancel()
    }
    
    private var clockRate: Int {
        switch streamRate {
        case .slowest: return 4
        case .slow   : return 7
        case .medium : return 14
        case .fast   : return 30
        case .fastest: return 60
        }
    }
    
    private var clockRateLeeway: Int {
        switch streamRate {
        case .slowest: return 16
        case .slow   : return 8
        case .medium : return 4
        case .fast   : return 2
        case .fastest: return 1
        }
    }
}

/*-------------------*/

public class EventBasedService: NetworkService {
    
    fileprivate class TCPServer: Server {
        
        struct ClientContext {
            let token: ClientToken
            let session: ETPSession
            
            init(remoteAddress: IPAddress) {
                token = ClientToken(remoteAddress: remoteAddress)
                session = ETPSession()
            }
        }
        
        fileprivate struct TCPClientInfo {
            let socket: TCPClientSocket
            var context: ClientContext
            
            init(socket: TCPClientSocket) {
                self.socket = socket
                self.context = ClientContext(remoteAddress: socket.remoteAddress)
            }
        }

        weak var delegate: ServerDelegate?
        private var runLoop: SocketRunLoop?
        private let socket: TCPServerSocket
        private var clientInfos: [ClientToken: TCPClientInfo] = [:]
        let port: PortID
        private let emptyData = Data()
        
        init(port: PortID) throws {
            self.port = port
            guard let socket = TCPServerSocket(port: port) else {
                throw ServiceError.internalError(message: "could not create tcp socket")
            }
            socket.isNonBlockingEnabled = true
            self.socket = socket
        }
        
        var connectionType: ConnectionType {
            return .tcp
        }
        
        func start(withRunLoop runLoop: SocketRunLoop) throws {
            try socket.bind()
            print("TCP server socket: bound")
            
            try socket.listen()
            print("TCP server socket: started to listen")
            
            try runLoop.add(socket: socket)
            self.runLoop = runLoop
        }
        
        func stop() throws {
            // close all connections
            //            clientInfos.values.forEach {
            //                do {
            //                    try $0.socket.close()
            //                } catch (let error) {
            //                    let remoteString = String(describing: $0.socket.remoteAddress)
            //                    log(.warning, domain: TCPServiceDomain, log: "service failed to close client tcp connection. client=\(remoteString), error=\(error)")
            //                }
            //            }
            
            clientInfos = [:]
            try socket.close()
            print("TCP server: stopped")
        }
        
        func send(_ message: Message, toClient token: ClientToken) throws {
            guard let clientInfo = clientInfos[token] else { throw ServiceError.invalidClientToken }
            let archive = clientInfo.context.session.archiver.messageArchive(withMessage: message)
            try clientInfo.socket.write(archive)
            print("TCP service: sent message (type=\(message.type))")
        }
        
        func sendPing(toClient token: ClientToken) throws {
            guard let clientInfo = clientInfos[token] else { throw ServiceError.invalidClientToken }
            let pingArchive = clientInfo.context.session.archiver.pingArchive
            try clientInfo.socket.write(pingArchive)
            print("TCP service: sent ping")
        }
        
        func send(pong: Timestamp32, toClient token: ClientToken) throws {
            guard let clientInfo = clientInfos[token] else { throw ServiceError.invalidClientToken }
            let pongArchive = clientInfo.context.session.archiver.pongArchive(withTimestamp: pong)
            try clientInfo.socket.write(pongArchive)
            print("TCP service: sent pong")
        }
        
        func sendKeepAlive(toClient token: ClientToken) throws {
            guard let clientInfo = clientInfos[token] else { throw ServiceError.invalidClientToken }
            let keepAliveArchive = clientInfo.context.session.archiver.keepAliveArchive
            try clientInfo.socket.write(keepAliveArchive)
            print("TCP service: sent keep alive")
        }
        
        func disconnect(fromClient token: ClientToken) throws {
            guard let clientInfo = clientInfos[token] else { throw ServiceError.invalidClientToken }
            if let runLoop = runLoop {
                try runLoop.remove(socket: clientInfo.socket)
            }
            try clientInfo.socket.close()
            clientInfos[token] = nil
            print("TCP service: disconnected client")
        }
        
        private func onDidClientConnect(_ clientInfo: TCPClientInfo) {
            let token = clientInfo.context.token
            clientInfos[token] = clientInfo
            delegate?.onDidConnect(withClientToken: token)
        }
        
        private func onDidClientDisconnect(_ token: ClientToken) {
            clientInfos[token] = nil
            delegate?.onDidDisconnect(withClientToken: token)
        }
        
        private func processClientRead(clientSocket: TCPClientSocket, clientContext: ClientContext) {
            // monitor for new incoming data
            var readEnabled = true
            while readEnabled {
                do {
                    guard let headerArchive = try clientSocket.read(numberOfBytes: ETPTransportHeader.Layout.size) else {
                        // connection closed
                        onDidClientDisconnect(clientContext.token)
                        readEnabled = false
                        continue
                    }
                    
                    let header = try clientContext.session.unarchiver.unarchiveHeader(headerArchive)
                    
                    let transportValue: ETPTransportValue
                    if header.payloadSize == 0 {
                        transportValue = try clientContext.session.unarchiver.unarchiveValue(header: header)
                    } else {
                        guard let payload = try clientSocket.read(numberOfBytes: Int(header.payloadSize)) else {
                            // connection closed
                            onDidClientDisconnect(clientContext.token)
                            readEnabled = false
                            continue
                        }
                        transportValue = try clientContext.session.unarchiver.unarchiveValue(header: header, payload: payload)
                    }
                    
                    // incoming data
                    switch transportValue {
                    case .keepAlive:
                        log(.info, domain: ServiceDomain, log: "received keep alive")
                    case .ping(let timestamp):
                        guard let timestamp = timestamp else {
                            log(.warning, domain: ServiceDomain, log: "ping did not contain a timestamp")
                            continue
                        }
                        try send(pong: timestamp, toClient: clientContext.token)
                    case .pong(let duration):
                        guard let duration = duration else {
                            log(.warning, domain: ServiceDomain, log: "pong did not contain a duration")
                            continue
                        }
                        delegate?.onDidReceivePong(withDuration: duration, fromClient: clientContext.token)
                    case .message(let message):
                        delegate?.onDidReceive(message: message, fromClient: clientContext.token)
                    }
                    
                } catch SocketError.operationFailure(let errorCode) where errorCode == .WOULDBLOCK {
                    readEnabled = false
                } catch {
                    log(.warning, domain: ServiceDomain, log: "socket error during read. error=\(error)")
                    
                    // close connection- something went wrong
                    onDidClientDisconnect(clientContext.token)
                    readEnabled = false
                }
            }
        }
        
        func runLoop(_ runLoop: SocketRunLoop, socket: TCPServerSocket, didReceiveConnectionRequestFrom clientSocket: TCPClientSocket) {
            clientSocket.isNonBlockingEnabled = true
            let newClientInfo = TCPClientInfo(socket: clientSocket)
            onDidClientConnect(newClientInfo)
            do {
                try runLoop.add(socket: clientSocket, context: newClientInfo.context)
            } catch {
                log(.warning, domain: ServiceDomain, log: "failed to add connection on run loop")
            }
        }
        
        func runLoop(_ runLoop: SocketRunLoop, socket: TCPServerSocket, didCloseWithError errorCode: SocketErrorCode?) {
            if let errorCode = errorCode {
                log(.warning, domain: ServiceDomain, log: "server socket error (errorCode=\(errorCode))")
            }
        }
        
        func runLoop(_ runLoop: SocketRunLoop, socket: TCPClientSocket, didReceiveDataWithContext context: Any?) {
            guard let clientContext = context as? ClientContext else { return }
            processClientRead(clientSocket: socket, clientContext: clientContext)
        }
        
        func runLoop(_ runLoop: SocketRunLoop, socket: TCPClientSocket, didCloseWithError errorCode: SocketErrorCode?, context: Any?) {
            guard let clientContext = context as? ClientContext else { return }
            onDidClientDisconnect(clientContext.token)
            
            if let errorCode = errorCode {
                log(.warning, domain: ServiceDomain, log: "client socket error (errorCode=\(errorCode))")
            }
        }
        
        func runLoop(_ runLoop: SocketRunLoop, socketDidReceiveData socket: UDPServerSocket) {}
        func runLoop(_ runLoop: SocketRunLoop, socket: UDPServerSocket, didCloseWithError errorCode: SocketErrorCode?) {}
    }
    
    public weak var delegate: NetworkServiceDelegate? {
        didSet {
            _delegate = delegate
        }
    }
    
    public init(tcpPort port: PortID, name: String? = nil, callbackQueue: DispatchQueue = DispatchQueue.main) throws {
        let server = try TCPServer(port: port)
        super.init(server: server, name: name, callbackQueue: callbackQueue)
    }
}
